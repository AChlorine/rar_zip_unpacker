# -*- coding: utf-8 -*-
import os, zipfile, rarfile, logging, sys, subprocess


#GOTTA INSTALL WINRAR, THEN GIVE THE PATH TO THE UNRAR FILE
rarfile.UNRAR_TOOL = 'C:/Program Files/WinRAR/UnRAR.exe'

def setup_logger():
    logging.basicConfig(level=logging.DEBUG)
    return logging.getLogger(__name__)

def is_linux_if_not_windows():
    operating_system = os.environ.get('OS')
    if 'linux' in operating_system.lower():
        return True
    elif 'windows' in operating_system.lower():
        return False

def generate_paths(is_linux):
    os_paths = {'windows': 'C:/Users/{}/', 'linux': ''}
    source = ''
    target = ''
    user = os.environ.get('USERNAME')

    if not is_linux:
        source = os_paths['windows'].format(user) + 'Downloads/Test/'
        target = os_paths['windows'].format(user) + 'Desktop/test/'
    elif is_linux:
        source = os_paths['linux'].format(user)

    paths = {'source': source, 'target': target}
    return paths

def obtain_rars(files):
    rars = list(filter(lambda file: file[len(file)-4:] == '.rar', files))
    return rars
def obtain_zips(files):
    zips = list(filter(lambda file: file[len(file)-4:] == '.zip', files))
    return zips
def unzip_files(all_paths, logger):
    bad_files = []
    print('\nExtracting files...')
    for index, item in enumerate(all_paths['source_paths']):
        try:
            if item[len(item)-4:] == '.zip':
                with zipfile.ZipFile(item, 'r') as zip:
                    zip.extractall(all_paths['target_paths'][index].strip())
                    zip.close()
                    print(str(index) + '. ' + all_paths['all_names'][index] + ' has been successfully extracted!')
                    os.remove(item)
            elif item[len(item)-4:] == '.rar':
                with rarfile.RarFile(item, 'r') as rar:
                    rar.extract(all_paths['target_paths'][index].strip())
                    rar.close()
                    print(str(index) + '. ' + all_paths['all_names'][index] + ' has been successfully extracted!')
                    os.remove(item)
        except Exception as e:
                logger.exception(e)
                bad_files.append(all_paths['target_paths'][index])
                pass

    cleanup_delete_empty_folders(all_paths['target_paths'])

    if bad_files:
        print('The following files could not be extracted: ')
        for index, item in enumerate(bad_files):
            print(str(index) + '. ' + item)

    print("\nAll operations complete!")

def obtain_all_paths(parameters):
    print('Source: ' + parameters['source'])
    print('Target: ' + parameters['target'])

    all_source_paths = []
    all_target_paths = []
    all_names = parameters['rars'] + parameters['zips']

    print('Number of files: ' + str(len(all_names)) + '\n')

    for item in parameters['rars']:
        all_source_paths.append(parameters['source'] + item)
        all_target_paths.append(parameters['target'] + item[:len(item)-4])
    for item in parameters['zips']:
        all_source_paths.append(parameters['source'] + item)
        all_target_paths.append(parameters['target'] + item[:len(item)-4])

    all_paths = {'source_paths':all_source_paths, 'target_paths': all_target_paths, 'all_names': all_names}

    return all_paths

def remove_whitespaces_from_names(names):
    for index, item in enumerate(names):
        names[index] = item.strip()
    return names

def create_folders(all_paths):
    already_exists = []
    for item in all_paths['target_paths']:
        if not os.path.exists(item):
            print("\nCreating folder {}".format(item.rsplit('/', 1)[1]) + "...", end="")
            os.makedirs(item)
        else:
            already_exists.append(item)
    print()
    if already_exists:
        print('The following paths already exist: ')
        for index, item in enumerate(already_exists):
            print(str(index + 1) + '. ' + item)
def cleanup_delete_empty_folders(target_paths):
    for item in target_paths:
        if not os.listdir(item):
            print("Removing empty folder: " + item)
            os.rmdir(item)
def create_open_path_folder_dispatch():
    open_directory = lambda target: subprocess.Popen(f"explorer \"{os.path.realpath(target)}\"")
    nothing = lambda target: print(target)
    choices = {
        'y': open_directory,
        'n': nothing
    }
    return choices

def open_path_folder(target): 
    choices = create_open_path_folder_dispatch()
    print(f'\nDo you want to open the following folder: {target}? y/n')
    choice = input('Choice: ')

    if choice not in list(choices.keys()):
        print('Invalid choice')
    else:
        choices[choice](target)
        return True

def main():
    logger = setup_logger()
    #is_linux = is_linux_if_not_windows()
    #paths = generate_paths(is_linux)
    
    source = 'C:/Users/AChlorine/Desktop/Source/'
    target = 'C:/Users/AChlorine/Desktop/test/{}/'.format(sys.argv[1])
    
    files = os.listdir(source)
    rars = obtain_rars(files)
    zips = obtain_zips(files)
    parameters = {'rars': rars, 'zips': zips, 'source': source, 'target': target}
    all_paths = obtain_all_paths(parameters)
    all_paths['all_names'] = remove_whitespaces_from_names(all_paths['all_names'])
    create_folders(all_paths)
    unzip_files(all_paths, logger)
    open_path_folder(target)

if __name__ == '__main__':
    main()
