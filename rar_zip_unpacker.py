#THIS FILE IS DEPRECATED. IT'S ONLY IN THE REPO FOR THE SAKE OF COMPARISON, TO LET MET SEE HOW FAR I HAVE COME IN TERMS OF PROGRAMMING KNOWLEDGE
import zipfile #3rd party module
import os
import shutil
import rarfile #3rd party module


def unfuck_name(filtered):
        i = 0
        length = len(filtered)
        while(i < length):
            filtered[1][i] = filtered[1][i].strip()
            i += 1
        return filtered        
def stringlength(string, decrement):
        length = len(string)- decrement
        return length
def filterresults(result_list):
    #creates a list that contains two more lists,
    #one containing the name of the file without the .zip or .rar extension;
    #the other list contains names with extensions
    filtered_results = []
    namelist = []
    results_length = len(result_list)
    i = 0
    while(i<results_length):
        #gives you length of string without 'rar' or 'zip', excluding the '.' in the file name
        filterlength = stringlength(result_list[i],3)
        #gives you length of string without '.rar' or '.zip'
        namelength = stringlength(result_list[i],4)
        #gives you either 'zip' or 'rar' by removing all elements in a string, except the extension, excluding the '.'
        extension = result_list[i][filterlength:]
        
        if(extension == 'zip' or extension == 'rar'):
            filtered_results.append(result_list[i])
            namelist.append(result_list[i][:namelength])
        
        i += 1
    results = [namelist, filtered_results]
    return results

def createfolders(filtered):
    source_path = 'D:\\Source'
    target_path = 'D:\\Batch 01d\\ヤサカニ・アン' #Modify this 'D:\\Batch 01d\\New folder\\Alp',
                                                                  #'D:\\Downloads\\Yoko Litner\\batch'
    errors = []
    path3 = '' 
    path4 = ''
    namelistlength = len(filtered[0])
    i = 0
    filtered = unfuck_name(filtered)
    print("Source path: ", source_path)
    print("Destination: ", target_path)
    print("Amount of files: ", namelistlength) 
    #input("//NOT IMPLEMENTED// Change directories? y/n")
    while(i<namelistlength):
        try:
            path3 = target_path+'\\'+filtered[0][i]
            path3 = path3.strip()
            path4 = source_path+'\\'+filtered[1][i]
            path4 = path4.strip()
            os.makedirs(path3)
            zipobject = zipfile.ZipFile(path4,'r')
            zipobject.extractall(path3)
            zipobject.close()
            print(i, ". ", filtered[0][i], " successfully extracted.")
            os.remove(path4)
            i += 1
        except zipfile.BadZipfile: 
            print(i, ". ", filtered[0][i], " is rar file.")
            rarobject = rarfile.RarFile(path4,'r')
            rarobject.extractall(target_path+'\\'+filtered[0][i].strip())
            rarobject.close()
            print(filtered[0][i], " successfully extracted.")
            os.remove(path4)
            i += 1
            continue
        except NotImplementedError:
            print("Error at index ", i)
            print("Name: ", filtered[0][i])
            i += 1
            continue
        except rarfile.RarCreateError:
            print("RarCreateError at index ",i)
            print("Name: ", filtered[0][i])
            break
    print("Amount of files extracted: ", i)
    print("Operation Succeeded.")

source_path = 'D:\\Source' #no idea what this does
result_list = os.listdir(source_path) #gives you name of all elements in source path
filtered = filterresults(result_list)
createfolders(filtered)
#input("Press ENTER to close")
#TODO fix the filterresults function so that it processes .zip and .rar files separately
#(DONE, BUT CHECK JUST TO MAKE SURE) Also, find a way to make the createfolders function process a path set by the user, and incorporate the function to a class
#PROBLEMS WITH PATH, CHECK AND FIX, DON'T PASS PATH AS STRING AND REMOVE DOUBLE BACKSLASH
#FOR GET_PATH AND SET_PATH, USE PATH WITHOUT DOUBLE BACKSLASH AND QUOTES

#FileExistsError: [WinError 183] Cannot create a file when that file already exists:
